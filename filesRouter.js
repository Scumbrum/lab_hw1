const express = require('express');
const router = express.Router();
const { createFile, getFiles, getFile, editFile, deleteFile } = require('./filesService.js');

router.post('/', createFile);

router.get('/', getFiles);

router.put('/:filename', editFile)

router.delete('/:filename', deleteFile)

router.get('/:filename', getFile);

// Other endpoints - put, delete.

module.exports = {
  filesRouter: router
};
