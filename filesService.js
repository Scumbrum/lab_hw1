const { response } = require('express');
const fs = require('fs');
const path = require("path")
// constants...

function createFile (req, res, next) {
  const {content, filename, password} = req.body
  if(!content || !filename) {
    next({message:"Not specified content or filename",
  status: 400})
    return
  }
  if(!/.json|txt|yaml|xml|js$/.test(filename)) {
    next({message:"Not correct extension",
  status: 400})
    return
  }
  try {
    if(password !== undefined) {
      createPassword(filename, password)
    }
    fs.writeFile(`./files/${filename}`, content, {encoding:"utf8"}, () => {
      res.status(200).send({ "message": "File created successfully" });
    })
  } catch (err) {
    next({message:"Can't create a file",
          status: 500})
  }
  
}

function createPassword(filename, password) {
  if(password ==="" | password.length < 5) {
    throw new Error("Invalid password")
  }
  let content = fs.readFileSync(`./passwords.json`, {encoding:"utf8"})
  content = JSON.parse(content)
  content[filename]= password
  fs.writeFileSync(`./passwords.json`, JSON.stringify(content), {encoding:"utf8"})
}

function checkPassword(filename, password) {
  let content = fs.readFileSync(`./passwords.json`, {encoding:"utf8"})
  content = JSON.parse(content)
  if(!content[filename]) {
    return true
  }
  if(content[filename] !== password) {
    return false
  }
  return true
}

function getFiles (req, res, next) {
  try {
    fs.readdir("./files", (err, files) => {
      if(!err) {
        res.status(200).send({
          "message": "Success",
          "files": [...files]});
      } else {
        next({message:err.message,
          status: 500})
      }
    })
  } catch(e) {
    next(e)
  }
}

const getFile = (req, res, next) => {
  const filename = req.params.filename
  const password = req.query.password
  fs.readFile(`./files/${filename}`, {encoding:"utf8"}, (err, data) => {
    if(!err) {
      if(!checkPassword(filename, password)) {
        next(
          {message: "Not correct password",
          status: 400})
        return
      }
      fs.stat(`./files/${filename}`, (err, stats) => {
        if(!err) {
          res.status(200).send({
            "message": "Success",
            "filename": filename,
            "content": data,
            "extension": path.extname(filename).substring(1),
            "uploadedDate": stats.mtime});
        }
      })
    } else if( err.code==="ENOENT") {
      res.status(400).send({
        "message": "No such file"
      })
    } else {
      next(err.message)
    }
  })
}

const editFile = (req, res, next) => {
  const filename = req.params.filename
  const password = req.query.password
  const content = req.body.content
  const exists = fs.existsSync(`./files/${filename}`)
  if(!exists) {
    res.status(400).send({
      "message": "No such file"
    })
    return
  }
  if(!checkPassword(filename, password)) {
    next(
      {message: "Invalid password",
      status:400})
    return
  }
  try {
    fs.writeFile(`./files/${filename}`, content, {encoding:"utf8"}, () => {
      res.status(200).send({ "message": "File edited successfully" });
    })
  } catch (err) {
    next({message:"Can't edit a file",
          status:500})
  }
}

const deleteFile = (req, res, next) => {
  const filename = req.params.filename
  const password = req.query.password
  const exists = fs.existsSync(`./files/${filename}`)
  if(!exists) {
    res.status(400).send({
      "message": "No such file"
    })
    return
  }
  if(!checkPassword(filename, password)) {
    next({message: "Invalid password",
          status: 400})
    return
  }
  try {
    fs.unlink(`./files/${filename}`, () => {
      res.status(200).send({ "message": "File removed successfully" });
    })
  } catch (err) {
    next({
      message:"Can't remove a file",
      status: 500})
  }
}

module.exports = {
  createFile,
  getFiles,
  getFile,
  editFile,
  deleteFile
}
